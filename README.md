### We need to enable StatsD in airflow.cfg
```
[metrics]
# Enables sending metrics to StatsD.
statsd_on = True
statsd_host = localhost
statsd_port = 8125
statsd_prefix = airflow
```